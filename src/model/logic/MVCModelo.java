package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Iterator;

import model.data_structures.*;
import model.logic.*;;


/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo <T extends Comparable<T>>
{

	public final static String DATOS_LISTA ="./data/bogota-cadastral-2018-2-All-HourlyAggregate.csv";

	/**
	 * Atributos del modelo del mundo
	 */



	private IListaEnlazada<UBERTrip> datosLista;




	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{

		datosLista = new ListaEnlazada<UBERTrip>() ;


	}


	
	/**
	 * Servicio de consulta de numero de elementos presentes en el modelo 
	 * @return numero de elementos presentes en el modelo
	 */
	public int darTamanoLista()
	{
		return datosLista.size();
	}

	


	/**
	 * Requerimiento de agregar dato
	 * @param dato
	 */


	public void agregarEnLista(UBERTrip dato)
	{
		datosLista.agregar(dato);
	}



	/**
	 * Requerimiento eliminar dato
	 * @param dato Dato a eliminar
	 * @return dato eliminado
	 */
	public UBERTrip eliminarEnListaH (UBERTrip dato)
	{
		return (UBERTrip) datosLista.eliminar(dato);
	}




	/**
	 * 
	 * @return
	 */
	public UBERTrip primerViaje()
	{
		return datosLista.darPrimero();
	}



	/**
	 * 
	 * @return
	 */
	public UBERTrip UltimoViaje()
	{
		return datosLista.darUltimo();
	}


	/**
	 * 
	 * @return
	 */
	public int cargarDatos()
	{

		File archivo = new File(DATOS_LISTA);



		FileReader lector;

		int totalViajes=0;

		try {

			lector = new FileReader(archivo);


			BufferedReader bf = new BufferedReader(lector);

			String linea = bf.readLine();
			bf.readLine();

			while(linea != null )
			{
				String[] datos = linea.split(",");

				int sourceid = Integer.parseInt(datos[0]);
				int dstid = Integer.parseInt(datos[1]);
				int DHM = Integer.parseInt(datos[2]);
				double meanTravelTime = Double.parseDouble(datos[3]);
				double standardDeviationTravelTime =  Double.parseDouble(datos[4]);
				double geometricMeanTravelTime=  Double.parseDouble( datos[5]);
				double geometricStandardDeviationTravelTime=  Double.parseDouble(datos[6]);


				UBERTrip viaje = new UBERTrip(sourceid, dstid, DHM, meanTravelTime, standardDeviationTravelTime, geometricMeanTravelTime, geometricStandardDeviationTravelTime);


				datosLista.agregar(viaje);


				linea = bf.readLine();
				totalViajes++;


			}
		}
		catch (Exception e) {

			e.printStackTrace();
		}
		return totalViajes;
	}

	/**
	 * Recorremos toda la lista cargada, los viajes que considen con la hora dada por parametro se retorna
	 * 
	 * @param hora
	 * @return arreglo comparable 
	 */
	public UBERTrip[] viajesHoraDada (int hora)
	{

		ListaEnlazada<UBERTrip> datoshora = new ListaEnlazada<UBERTrip>();

		UBERTrip[] iterar = new UBERTrip[datosLista.size()];

		for (int i = 0; i < iterar.length; i++) 
		{
			if(iterar[i].darHora()==hora)
			{
				datoshora.agregar(iterar[i]);

			}
		}		
		UBERTrip[] miarray = new UBERTrip[datoshora.size()];

		return miarray;

	}


	
	
	
	
	
	/**
	 * METODO DE ORDENAMIENTO Shell SORT 
	 *Ordenar ascendentemente los viajes resultantes de la consulta del punto 3 (en el arreglo de objetos comparables) usando el algoritmo ShellSort.
	 * el algoritmo se obtuvo de ayuda de videos de youtube y de ejemplos del libro
	 * @param hora
	 * @return
	 */
	public UBERTrip[] shellShow (int hora)
	{
		return shellSort(hora);
	}
	
	/**
	 * 
	 * @param hora
	 * @return
	 */
	public UBERTrip[] shellSort(int hora)
	{
		
		UBERTrip[] listaConViajes = new UBERTrip[viajesHoraDada(hora).length];
		
		int tamanoArreglo;
		int b;
		UBERTrip c;
		
		boolean paso;
		
		tamanoArreglo= listaConViajes.length;
		
		while(tamanoArreglo>0)
		{
			tamanoArreglo = tamanoArreglo/2;
			
			paso =true;
			
			while(paso==true)
			{
				paso= false;
				b = 0;
				
				while ((b + tamanoArreglo) <= listaConViajes.length-1)
				{
					if (listaConViajes[b].compareTo(listaConViajes[b + tamanoArreglo])==1 )
					{
						c = listaConViajes[b];
						listaConViajes[b] = listaConViajes[b+tamanoArreglo];
						listaConViajes[b+tamanoArreglo] = c;
						paso = true;
					}
					b = b +1;
				}
				
			}
		}
		return listaConViajes;
	}
	
	



	/**
	 * METODO DE ORDENAMIENTO MERGE SORT 
	 *Ordenar ascendentemente los viajes resultantes de la consulta del punto 3 (en el arreglo de objetos comparables) usando el algoritmo Merge sort.
	 * el algoritmo se obtuvo de ayuda de videos de youtube y de ejemplos del libro
	 * @param hora
	 * @return
	 */
	public UBERTrip[] mergeShow(int hora)
	{
		UBERTrip[] listaConDatos = new UBERTrip[viajesHoraDada(hora).length];
		
		
	
		return mergeSort(0, listaConDatos.length-1, hora);
	}
	
	
	/**
	 * 
	*Ordenar ascendentemente los viajes resultantes de la consulta del punto 3 (en el arreglo de objetos comparables) usando el algoritmo MergeSort.
	 * @param plow
	 * @param phigh
	 * @param hora
	 * @return
	 */
	public UBERTrip[] mergeSort( double plow, double phigh, int hora)
	{

		UBERTrip[] aux = new UBERTrip[viajesHoraDada(hora).length];
		
		if(plow >= phigh)
		{
			return aux;
		}

		double  middle = (plow+ phigh)/2;

		mergeSort( plow, middle,hora);

		mergeSort( middle+1, phigh, hora);

		merge (  plow,middle, phigh, hora);
		
		return aux;

 
	}

	/**
	 * 
	 * @param Plow
	 * @param Pmiddle
	 * @param Phigh
	 * @param hora
	 */
	private void merge ( double Plow, double Pmiddle, double Phigh, int hora )
	{
		UBERTrip[] listaAOrdenar = new UBERTrip[viajesHoraDada(hora).length];

		UBERTrip[] aux = new UBERTrip[viajesHoraDada(hora).length];
		
		double i = Plow;
		double j = Pmiddle+1;
		double k = Plow;

		while ((i <= Pmiddle) &&  (j <= Phigh))
		{   
			if (listaAOrdenar[(int) i].compareTo(listaAOrdenar[(int) j])==(-1|0)){

				listaAOrdenar[(int) k]= aux[(int) i];
				i++;
			}

			else{
				
				listaAOrdenar[(int) k]= aux[(int) j];
				j++;
			}
		}
		
		k++;

		while(i<= Pmiddle) {
			listaAOrdenar[(int) k]= aux[(int) i];
			k++;
			i++;
		}

		while(j<= Phigh) {
			listaAOrdenar[(int) k]= aux[(int) j];
			k++;
			j++;
		}
	}

	
	
	

	/**
	 * METODO DE ORDENAMIENTO QUICK SORT 
	 *Ordenar ascendentemente los viajes resultantes de la consulta del punto 3 (en el arreglo de objetos comparables) usando el algoritmo QuickSort.
	 * el algoritmo se obtuvo de ayuda de videos de youtube y de ejemplos del libro
	 * @param hora
	 * @return
	 */
	public UBERTrip[] quickShow ( int hora)
	{
		UBERTrip[] listaConDatos = new UBERTrip[viajesHoraDada(hora).length];
		
		return quickSort(0, listaConDatos.length-1, hora);
	}
	
	/**
	 * 
	 * @param izq
	 * @param der
	 * @param hora
	 * @return
	 */
	public UBERTrip[] quickSort(int izq, int der, int hora)
	{
		
		UBERTrip[] listaConDatos = new UBERTrip[viajesHoraDada(hora).length];
		
		
		
		if(listaConDatos[izq].compareTo(listaConDatos[der]) ==(1|0)){
			return listaConDatos;
		}
	
		int i =  izq ;
		int d = der;
		
		if(izq!=der)
		{
			
			UBERTrip pivote = listaConDatos[0];
			
			UBERTrip aux;
			
			pivote = (UBERTrip)listaConDatos[izq];
			
			while(izq!=der)
			{
				
				while ((listaConDatos[der].compareTo(pivote) == (1|0) ) && listaConDatos[izq].compareTo(listaConDatos[der])==1)
				{
					der--;
				}
				
				while( listaConDatos[izq].compareTo(pivote) ==-1    && listaConDatos[izq].compareTo(listaConDatos[der])==1)
				{
					izq ++;
				}
				
				if(der!=izq)
				{
					aux = listaConDatos[der];
					
					listaConDatos[der]= listaConDatos[izq];
					
					listaConDatos[izq] = aux ;
					
				}
				
				if(der==izq)
				{
					quickSort(i, izq-1,hora);
					quickSort(izq+1, d,hora);
				}
			}
			
			
		}
		return listaConDatos;
		
	}







}
