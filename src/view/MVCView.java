package view;

import model.logic.MVCModelo;

public class MVCView 
{
	    /**
	     * Metodo constructor
	     */
	    public MVCView()
	    {
	    	
	    }
	    
		public void printMenu()
		{
			System.out.println("1. cargar datos");
			System.out.println("2. Consultar viajes para una hora dada");
			System.out.println("3. Ordenar con Shell Sort");
			System.out.println("4. Ordenar con Merge Sort");
			System.out.println("5. Ordenar con Quick Sort");
			System.out.println("6. Medir la eficiencia de los 3 algoritmos");
			System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
		}

		public void printMessage(String mensaje) {

			System.out.println(mensaje);
		}		
		
		public void printModelo(MVCModelo modelo)
		{
			System.out.println(modelo);		
		}
		
		
		
		
		
		
}
