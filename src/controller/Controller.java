package controller;

import java.util.Scanner;

import model.logic.MVCModelo;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}

	public void run() 
	{
		
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";
		
		dato = lector.next();
		int hora = Integer.parseInt(dato);
		
		
		

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:

			
				view.printMessage("Se empezaron a cargar los datos");

				dato = lector.next();

				modelo.cargarDatos();

				view.printMessage("El total de viajes es:"+ modelo.cargarDatos());


				view.printMessage("la zona  de origen del primer viaje es"+modelo.primerViaje().darsourceId()+"La zona de destino es" +modelo.primerViaje().dardestinID()+" La hora del vije es " + modelo.primerViaje().darHora()+"El tiempo promedio del viaje es" + modelo.primerViaje().darmeanTravelTime()  );
				view.printMessage("la zona  de origen del ultimo viaje es"+modelo.UltimoViaje().darsourceId()+"La zona de destino es" +modelo.UltimoViaje().dardestinID()+" La hora del vije es " + modelo.UltimoViaje().darHora()+"El tiempo promedio del viaje es" + modelo.UltimoViaje().darmeanTravelTime()  );


				break;

			case 2:

				if(hora>0&&hora<23)
				{
				view.printMessage("digite la hora que desea cargar(la hora debe ser un valor entero)");

				modelo.viajesHoraDada(hora);

				view.printMessage("El numero de viajes de la consulta son:"+ modelo.viajesHoraDada(hora).length);
				}
				else
					view.printMessage("La hora ingresada no es valida");
				
				break;

			case 3:
				
				if(hora>0&&hora<23)
				{
					view.printMessage("Se estan ordenando Ascendentemente los viajes resultantes de la consulta del punto 2");
					
					long startTimeS = System.currentTimeMillis();				
					modelo.shellShow(hora);
					long endTimeS = System.currentTimeMillis() - startTimeS;
					view.printMessage("El algoritmo se tomo:"+ endTimeS+"milisegundos para ordenar los viajes");
					
					for (int i = 0; i < 10 ; i++) //cambiar 10
					{
						view.printMessage("Viaje numero:"+i+ modelo.shellShow(hora)[i] );	
					}
					
					for (int i = modelo.shellShow(hora).length ; i > 10 ; i--) //cambiar 10
					{
						view.printMessage("Viaje numero:"+i+ modelo.shellShow(hora)[i] );	
					}	
				}
				else
					view.printMessage("La hora ingresada no es valida");
				
				
				break;

			case 4:


				if(hora>0&&hora<23)
				{
					view.printMessage("Se estan ordenando Ascendentemente los viajes resultantes de la consulta del punto 2");
					
					
					long startTimeM = System.currentTimeMillis();				
					modelo.mergeShow(hora);
					long endTimeM = System.currentTimeMillis() - startTimeM;
					
					view.printMessage("El algoritmo se tomo:"+ endTimeM+"milisegundos para ordenar los viajes");
					
					
					for (int i = 0; i < 10 ; i++) //cambiar 10
					{
						view.printMessage("Viaje numero:"+i+ modelo.mergeShow(hora)[i] );	
					}
					
					for (int i = modelo.mergeShow(hora).length ; i > 10 ; i--) //cambiar 10
					{
						view.printMessage("Viaje numero:"+i+ modelo.mergeShow(hora)[i] );	
					}
				
					
					
				}
				else
					view.printMessage("La hora ingresada no es valida");
				
				break;

			case 5:

				
				if(hora>0&&hora<23)
				{
				
					
					view.printMessage("Se estan ordenando Ascendentemente los viajes resultantes de la consulta del punto 2");
					
					long startTimeQ = System.currentTimeMillis();				
					modelo.quickShow(hora);
					long endTimeQ = System.currentTimeMillis() - startTimeQ;
					
					view.printMessage("El algoritmo se tomo:"+ endTimeQ+"milisegundos para ordenar los viajes");
					
					for (int i = 0; i < 10 ; i++) //cambiar 10
					{
						view.printMessage("Viaje numero:"+i+ modelo.quickShow(hora)[i] );	
					}
					
					for (int i = modelo.quickShow(hora).length ; i > 10 ; i--) //cambiar 10
					{
						view.printMessage("Viaje numero:"+i+ modelo.quickShow(hora)[i] );	
					}
					
				}
				else
					view.printMessage("La hora ingresada no es valida");
				


				break;

			case 6:

				
				if(hora>0&&hora<23)
				{
					view.printMessage("Se compararan los algoritmos ");
					
					//Los datos obtenidos de una consulta por la hora del día (valor entero en el rango [0, 23]) 
					//están desordenados. Se quiere comparar la eficiencia en tiempo de los tres algoritmos de ordenamiento para los viajes UBER 
					//resultado de una misma consulta por la hora del día. Para este objetivo, 
					//hay que cronometrar el tiempo de ejecución (en milisegundos) de cada algoritmo de ordenamiento.
					
					
					long startTime1 = System.currentTimeMillis();				
					modelo.shellShow(hora);
					long endTime1 = System.currentTimeMillis() - startTime1;
					
					long startTime2 = System.currentTimeMillis();				
					modelo.mergeShow(hora);
					long endTime2 = System.currentTimeMillis() - startTime2;
					
					long startTime3 = System.currentTimeMillis();				
					modelo.quickShow(hora);
					long endTime3 = System.currentTimeMillis() - startTime3;
					
					view.printMessage(
					"Al el algoritmo de ShellSort le tomo"+ endTime1 +" milisegundos realizar la busqueda"+
					"Al el algoritmo de MergeSort le tomo"+ endTime2 +" milisegundos realizar la busqueda"+ 
					"Al el algoritmo de QuickSort le tomo"+ endTime3 +" milisegundos realizar la busqueda");
					
				}
				else
					view.printMessage("La hora ingresada no es valida");
				
				break;




			default: 
				System.out.println("--------- \n Opcion Invalida !! \n---------");
				break;
			}
		}

	}	
}
